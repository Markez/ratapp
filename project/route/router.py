from project.controllers.main.controllers import main
from project.controllers.admin.controllers import admin
from project.configs.configs import app


app.register_blueprint(main, url_prefix='/')
app.register_blueprint(admin, url_prefix='/admin')
