# -*- coding: utf-8 -*-
from flask import render_template, request
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired
from flask import Blueprint
from project.models.Printer import Printer


admin = Blueprint('admin', __name__)


class CreateForm(FlaskForm):
    text = StringField('name', validators=[DataRequired()])


@admin.route('/', methods=['GET', 'POST'])
def printer():
    form = CreateForm(request.form)
    if request.method == 'POST' and form.validate():
        printer = Printer()
        printer.show_string(form.text.data)
        return render_template('printer/index.html')
    return render_template('printer/print.html', form=form)
