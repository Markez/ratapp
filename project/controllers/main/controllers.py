# -*- coding: utf-8 -*-
from flask import render_template
from flask import Blueprint


main = Blueprint('main', __name__)


@main.route('/')
def start():
    return render_template('printer/index.html')